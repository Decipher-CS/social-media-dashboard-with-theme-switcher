let theme = localStorage.getItem('theme');
let checkbox = document.querySelector("#theme-toggle-checkbox");
let overviewLight = document.querySelector(".overview")
let dashboardLight = document.querySelector(".card-wrapper-dashboard")
let bd = document.getElementsByTagName("body")[0]

// const switchThemes = () =>{
//     document.body.classList.add('theme');
//     localStorage.setItem("theme", "light")
// }

checkbox.addEventListener("click", () => {

    if (checkbox.checked == true) {
        bd.classList.add("body-light")
        overviewLight.classList.add("overview-light")
        dashboardLight.classList.add("light-theme-dashboard")
    }
    else {
        bd.classList.remove("body-light")
        overviewLight.classList.remove("overview-light")
        dashboardLight.classList.remove("light-theme-dashboard")
    }
});